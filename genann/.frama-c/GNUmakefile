# Makefile template for Frama-C/Eva case studies.
# For details and usage information, see the Frama-C User Manual.

### Prologue. Do not modify this block. #######################################
-include path.mk
FRAMAC ?= frama-c
include $(shell $(FRAMAC)-config -print-lib-path)/analysis-scripts/prologue.mk
###############################################################################

# Edit below as needed. Suggested flags are optional.

MACHDEP = gcc_x86_64

# The source code uses the __FILE__ macro; to obtain stable oracles, we use
# a specific GCC option based on the directory containing this Makefile.
mkfile_path := $(abspath $(firstword $(MAKEFILE_LIST))/../..)

## Preprocessing flags (for -cpp-extra-args)
CPPFLAGS    += \
  -ffile-prefix-map=$(mkfile_path)=.

## General flags
FCFLAGS     += \
  -add-symbolic-path=..:. \
  -kernel-warn-key annot:missing-spec=abort \
  -kernel-warn-key typing:implicit-function-declaration=abort \

## Eva-specific flags
EVAFLAGS    += \
  -eva-warn-key builtins:missing-spec=abort \
  -eva-slevel 9 \

## GUI-only flags
FCGUIFLAGS += \

## Analysis targets (suffixed with .eva)
TARGETS = genann.eva

### Each target <t>.eva needs a rule <t>.parse with source files as prerequisites
genann.parse: \
  ../genann.c \
  ../test.c \

### Epilogue. Do not modify this block. #######################################
include $(shell $(FRAMAC)-config -print-lib-path)/analysis-scripts/epilogue.mk
###############################################################################

# optional, for OSCS
-include ../../Makefile.common
