| Directory | Description | URL | #Eva targets | #.c/.h files | #LoC | Tags |
| --------- | ----------- | --- |          --- |          --- |  --- | ---  |
|2048|Console version of the game 2048|https://github.com/mevdschee/2048.c|1|1|367|NO_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|basic-cwe-examples|Small test cases based on "buggy" CWE demonstrative examples|https://cwe.mitre.org/data/|17|10|309|HAS_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|bench-moerman2018|Test suite used to evaluate static analysis tools|https://github.com/JMoerman/JM2018TS/|400|433|18532|HAS_DYN_ALLOC, UNUSED_RECURSION, NO_FLOAT|
|c-testsuite|Part of a collaborative database of compiler test cases|https://github.com/c-testsuite/c-testsuite|215|220|4959|HAS_DYN_ALLOC, HAS_RECURSION, HAS_FLOAT|
|c-utils|c-utils is a collection of utility libraries.|https://github.com/barrust/c-utils|13|43|6392|HAS_DYN_ALLOC, HAS_RECURSION, HAS_FLOAT|
|cerberus|Test suite related to the Cerberus project|https://www.cl.cam.ac.uk/~pes20/cerberus/|192|191|2520|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|chrony|Implementation of the Network Time Protocol|https://chrony.tuxfamily.org/|2|122|25455|HAS_DYN_ALLOC, REM_RECURSION, HAS_FLOAT|
|debie1|Benchmark based on the DEBIE-1 satellite instrument|http://www.tidorum.fi/debie1/|1|48|6269|NO_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|genann|Simple neural network library|https://github.com/codeplea/genann|1|8|690|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|gnugo|Go game|https://ftp.gnu.org/gnu/gnugo/|1|27|2048|NO_DYN_ALLOC, HAS_RECURSION, NO_FLOAT|
|gzip124|Data compression program|https://www.gnu.org/software/gzip/|1|35|5826|HAS_DYN_ALLOC, REM_RECURSION, NO_FLOAT|
|hiredis|Minimalistic client library for the Redis database|https://github.com/redis/hiredis|2|45|6622|HAS_DYN_ALLOC, REM_RECURSION, HAS_FLOAT|
|icpc|Fake application developed for a programming challenge, for ICPC2011|http://icpc2011.cs.usask.ca/conf_site/IndustrialTrack.html|1|15|965|NO_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|ioccc|Some winners of the International Obfuscated C Code Contest (IOCCC)|https://github.com/ioccc-src/winner|21|55|3909|HAS_DYN_ALLOC, UNUSED_RECURSION, HAS_FLOAT|
|itc-benchmarks|Static analysis benchmark from Toyota ITC|https://github.com/regehr/itc-benchmarks|2|107|29633|HAS_DYN_ALLOC, REM_RECURSION, HAS_FLOAT|
|jsmn|Minimalistic JSON parser|https://github.com/zserge/jsmn|2|7|806|UNUSED_DYN_ALLOC, UNUSED_RECURSION, NO_FLOAT|
|kgflags|Header-only command-line parsing library|https://github.com/kgabis/kgflags|2|5|1222|NO_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|khash|Standalone, generic C library|https://github.com/attractivechaos/klib|1|2|308|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|kilo|Text editor in less than 1000 lines of code|https://github.com/antirez/kilo/blob/master/kilo.c|1|2|1012|HAS_DYN_ALLOC, REM_RECURSION, NO_FLOAT|
|libmodbus|Library to send/receive data according to the Modbus protocol|https://github.com/stephane/libmodbus|2|22|5172|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|libspng|Library for writing and reading PNG files|https://github.com/randy408/libspng|1|5|3337|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|libyaml|Library for parsing and emitting YAML|https://github.com/yaml/libyaml|2|25|10548|HAS_DYN_ALLOC, UNUSED_RECURSION, NO_FLOAT|
|line-following-robot|Very simple AVR-based robot (student project)|https://github.com/Fabio-Morais/Line-Follower-Robot|1|21|3560|NO_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|mbedtls|Implementation of the TLS and SSL protocols (formerly PolarSSL)|https://github.com/Mbed-TLS/mbedtls|179|423|138478|HAS_DYN_ALLOC, HAS_RECURSION, NO_FLOAT|
|microstrain|Driver for a LORD/Microstrain Inertial Measurement Unit (IMU) sensor|https://github.com/ros-drivers/microstrain_mips|1|69|19075|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|mini-gmp|Implementation of a subset of GNU's Multiple Precision Arithmetic library|https://gmplib.org/|1|35|8686|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|miniz|Compression library in a single source file, implementing zlib and Deflate|https://github.com/richgel999/miniz|6|26|8299|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|monocypher|Small, self-contained cryptographic library|https://monocypher.org|1|42|23680|HAS_DYN_ALLOC(tests), NO_RECURSION, NO_FLOAT|
|papabench|Real time benchmark based on the Paparazzi unmanned aerial vehicle (UAV) code|https://github.com/t-crest/patmos-benchmarks/tree/master/PapaBench-0.4|1|80|7251|NO_DYN_ALLOC, UNUSED_RECURSION, HAS_FLOAT|
|polarssl|Implementation of the TLS and SSL protocols (formerly PolarSSL)|https://github.com/ARMmbed/mbedtls|1|119|29184|HAS_DYN_ALLOC, REM_RECURSION, NO_FLOAT|
|powerwindow|Distributed power window control, by CoSys-Lab, part of TACLeBench|https://github.com/tacle/tacle-bench/tree/master/bench/app/powerwindow|1|39|2918|NO_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|qlz|Fast compression library|http://www.quicklz.com|4|7|963|HAS_DYN_ALLOC(tests), NO_RECURSION, NO_FLOAT|
|safestringlib|Library for handling strings securely|https://github.com/intel/safestringlib|1|148|14922|NO_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|semver|Parser and renderer for semantic versioning 2.0.0|https://github.com/h2non/semver.c|1|4|1034|HAS_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|solitaire|Implementation of Bruce Schneier's card cipher, Solitaire (aka Pontifex)|http://www.ciphergoth.org/crypto/solitaire/|1|1|263|NO_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|stmr|Martin Porter's Stemming algorithm as a C library|https://github.com/wooorm/stmr.c|1|3|497|HAS_DYN_ALLOC, HAS_RECURSION, NO_FLOAT|
|tsvc|Test suite for vectorizing compilers|http://polaris.cs.uiuc.edu/~maleki1/TSVC.tar.gz|1|2|4041|HAS_DYN_ALLOC, NO_RECURSION, HAS_FLOAT|
|tweetnacl-usable|Cryptographic library in 100 tweets, in a more usable form|https://twitter.com/TweetNaCl|1|5|1072|HAS_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|verisec|Buffer overflow benchmark|https://security.csl.toronto.edu/wp-content/uploads/2018/06/Verisec-ASE2007.pdf|275|320|14561|HAS_DYN_ALLOC, NO_RECURSION, NO_FLOAT|
|x509-parser|RTE-free X.509 parser|https://github.com/ANSSI-FR/x509-parser|1|3|5057|NO_DYN_ALLOC, NO_RECURSION, NO_FLOAT|

Note: the 'Tags' column refers to code features that may be found in the code.
      A few examples are presented below.

- REM_RECURSION: the code contains recursive calls, but they were removed or
  commented in the current Eva test cases.
- UNUSED_RECURSION: the code contains recursive calls (either in the main
  application/library, or in test cases), but the current Eva targets do
  not call them.
